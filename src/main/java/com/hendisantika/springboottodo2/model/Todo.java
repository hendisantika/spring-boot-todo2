package com.hendisantika.springboottodo2.model;

import org.springframework.data.annotation.Id;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-todo2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-21
 * Time: 11:27
 */
public class Todo {
    @Id
    private String id;
    private String todo;
    private boolean isComplete;

    public Todo(String todo, boolean isComplete) {
        this.todo = todo;
        this.isComplete = isComplete;
    }

    public Todo() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTodo() {
        return todo;
    }

    public void setTodo(String todo) {
        this.todo = todo;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }
}
