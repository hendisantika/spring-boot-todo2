package com.hendisantika.springboottodo2.controller;

import com.hendisantika.springboottodo2.model.Todo;
import com.hendisantika.springboottodo2.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-todo2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-21
 * Time: 11:31
 */
@RestController
@RequestMapping("/api")
public class TodoController {

    @Autowired
    private TodoRepository todoRepository;

    @GetMapping("/todos")
    public Map<String, Object> getAllTodos() {
        List<Todo> todoList = todoRepository.findAll();
        Map<String, Object> response = new LinkedHashMap<>();
        response.put("totalTodos", todoList.size());
        response.put("todo", todoList);
        return response;
    }

    @PostMapping("/todo")
    public Map<String, Object> createTodo(@RequestBody Map<String, Object> todoMap) {
        Todo todo = new Todo(todoMap.get("todo").toString(),
                Boolean.parseBoolean(todoMap.get("isComplete").toString()));

        Map<String, Object> response = new LinkedHashMap<>();
        response.put("message", "Todo created successfully");
        response.put("todo", todoRepository.save(todo));
        return response;
    }

    @PutMapping("/todo/{id}")
    public Map<String, Object> updateTodo(@PathVariable("id") String id, @RequestBody Map<String, Object> todoMap) {
        Todo todo = new Todo(todoMap.get("todo").toString(),
                Boolean.parseBoolean(todoMap.get("isComplete").toString()));

        todo.setId(id);

        Map<String, Object> response = new LinkedHashMap<>();
        response.put("message", "Todo updated successfully");
        response.put("todo", todoRepository.save(todo));
        return response;
    }

    @GetMapping("/todo/{id}")
    public Map<String, Object> getOneTodo(@PathVariable("id") String id) {
        Optional<Todo> todo = todoRepository.findById(id);
        Map<String, Object> response = new LinkedHashMap<>();
        response.put("todo", todo);
        return response;
    }

    @DeleteMapping("/todo/{id}")
    public Map<String, Object> deleteTodo(@PathVariable("id") String id) {
        Todo todo = new Todo();
        todo.setId(id);
        todoRepository.delete(todo);

        Map<String, Object> response = new LinkedHashMap<>();
        response.put("message", "Todo deleted successfully");
        return response;
    }


}