package com.hendisantika.springboottodo2.repository;

import com.hendisantika.springboottodo2.model.Todo;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-todo2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-21
 * Time: 11:28
 */
public interface TodoRepository extends MongoRepository<Todo,String> {
}
