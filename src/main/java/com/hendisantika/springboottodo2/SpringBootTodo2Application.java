package com.hendisantika.springboottodo2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootTodo2Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootTodo2Application.class, args);
    }

}
