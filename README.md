# Spring Boot ToDo 

#### Spring Boot Backend Service with MongoDB - Todo Application

#### Things to do  to run locally:

1. Clone this repo
    ```
    git clone https://gitlab.com/hendisantika/spring-boot-todo2.git
    ```
2. Go to the folder : `cd spring-boot-todo2`

3. Run the app :    
    ```
    mvn clean spring-boot:run
    ```
#### Screen shot

##### Add New ToDo

![Add New Todo](img/add.png "Add New Todo")

##### List All ToDos

![List All ToDos](img/list.png "List All ToDos")

##### Find Todo By Id

![Find Todo By Id](img/find.png "Find Todo By Id")

##### Update Todo By Id

![Update Todo By Id](img/update.png "Update Todo By Id")

##### Delete Todo

![Delete Todo By Id](img/delete.png "Delete Todo By Id")
